{% for membership in object.members.all %}* {{ membership.user }} {% if membership.user.username != membership.user.name %}({{ membership.user.username }}){% endif %}
{% empty %}NONE
{% endfor %}
